package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.List;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    private List<Spell> spells;

    public ChainSpell(List<Spell> spells) {
        this.spells = spells;
    }

    @Override
    public void cast() {

        for (Spell spell : this.spells) {
            spell.cast();
        }
    }

    @Override
    public void undo() {
        for (int ii = this.spells.size() - 1; ii >= 0; ii--) {
            this.spells.get(ii).undo();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
