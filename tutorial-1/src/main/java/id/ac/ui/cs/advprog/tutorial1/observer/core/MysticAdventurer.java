package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {

    public MysticAdventurer(Guild guild) {
        this.name = "Mystic";
        //ToDo: Complete Me
        this.guild = guild;
        this.guild.add(this);
    }

    //ToDo: Complete Me

    @Override
    public void update() {
        String questType = guild.getQuestType();
        if(questType.compareTo("D") == 0 || questType.compareTo("E") == 0) {
            addQuest(guild.getQuest());
        }
    }
}
