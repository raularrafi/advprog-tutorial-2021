package id.ac.ui.cs.advprog.tutorial3.facade.core.adapter;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.CodexTranslator;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.Transformation;

public class TranslatorAdapter implements Transformation {

    private CodexTranslator wrappee;

    public TranslatorAdapter(CodexTranslator wrappee) {
        this.wrappee = wrappee;
    }

    @Override
    public Spell encode(Spell spell) {
        // Encodes to Runic
        return wrappee.translate(spell, RunicCodex.getInstance());
    }

    @Override
    public Spell decode(Spell spell) {
        // Decodes to Alpha
        return wrappee.translate(spell, AlphaCodex.getInstance());
    }
}

