package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.BowRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.LogRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.SpellbookRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.WeaponRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.LinkedList;

// TODO: Complete me. Modify this class as you see fit~
@Service
public class WeaponServiceImpl implements WeaponService {

    // feel free to include more repositories if you think it might help :)

    @Autowired
    private LogRepository logRepository;
    @Autowired
    private BowRepository bowRepository;
    @Autowired
    private SpellbookRepository spellbookRepository;
    @Autowired
    private WeaponRepository weaponRepository;

    // TODO: implement me
    @Override
    public List<Weapon> findAll() {
        List<Weapon> allWeaponList = new LinkedList<Weapon>();

        for (Weapon weapon : weaponRepository.findAll()) {
            allWeaponList.add(weapon);
        }

        for (Bow bow : bowRepository.findAll()) {
            if (weaponRepository.findByAlias(bow.getName()) == null) {
                allWeaponList.add(new BowAdapter(bow));
            }
        }

        for (Spellbook spellbook : spellbookRepository.findAll()) {
            if (weaponRepository.findByAlias(spellbook.getName()) == null) {
                allWeaponList.add(new SpellbookAdapter(spellbook));
            }
        }

        return allWeaponList;
    }

    // TODO: implement me
    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        Weapon weapon;
        String atkResult;

        if (weaponRepository.findByAlias(weaponName) != null) {
            weapon = weaponRepository.findByAlias(weaponName);
            atkResult = handleAttackType(weapon, attackType);

        } else if (spellbookRepository.findByAlias(weaponName) != null) {
            Spellbook spellbook = spellbookRepository.findByAlias(weaponName);
            weapon = new SpellbookAdapter(spellbook);
            atkResult = handleAttackType(weapon, attackType);

        } else if (bowRepository.findByAlias(weaponName) != null) {
            Bow bow = bowRepository.findByAlias(weaponName);
            weapon = new BowAdapter(bow);
            atkResult = handleAttackType(weapon, attackType);

        } else {
            return;
        }

        weaponRepository.save(weapon);

        logRepository.addLog(atkResult);
    }

    // TODO: implement me
    @Override
    public List<String> getAllLogs() {
        return logRepository.findAll();
    }
    private String handleAttackType(Weapon weapon, int attackType) {
        String logBeginning = weapon.getHolderName()
                + " attacked with "
                + weapon.getName();

        switch (attackType) {
            case 1:
                return logBeginning + " (normal attack) " + weapon.normalAttack();
            case 2:
                return logBeginning + " (charged attack) " + weapon.chargedAttack();
            default:
                return null;
        }
    }
}
