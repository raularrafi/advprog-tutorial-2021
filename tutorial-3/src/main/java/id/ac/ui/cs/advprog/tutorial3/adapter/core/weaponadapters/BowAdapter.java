package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

// TODO: complete me :)
public class BowAdapter implements Weapon {

    private Bow wrappee;
    private boolean isAimShot = false;

    public BowAdapter(Bow wrappee) {
        this.wrappee = wrappee;
    }

    @Override
    public String normalAttack() {
        return wrappee.shootArrow(isAimShot);
    }

    @Override
    public String chargedAttack() {
        if (isAimShot) {
            isAimShot = false;
            return "Leaving aim shot mode";
        } else {
            isAimShot = true;
            return "Enter aim shot mode";
        }
    }

    @Override
    public String getName() {
        return wrappee.getName();
    }

    @Override
    public String getHolderName() {
        // TODO: complete me
        return wrappee.getHolderName();
    }
}
