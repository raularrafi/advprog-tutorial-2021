package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

public class CaesarTransformation implements Transformation{
    private int jump;

    public CaesarTransformation() {
        this.jump = 1;
    }

    public CaesarTransformation(int jump) {
        this.jump = jump;
    }

    public Spell encode(Spell spell){
        return process(spell, true);
    }

    public Spell decode(Spell spell){
        return process(spell, false);
    }

    private Spell process(Spell spell, boolean encode) {
        StringBuilder result = new StringBuilder();

        int jumpAmt = encode ? jump : -1 * jump;

        for (char ch : spell.getText().toCharArray()) {
            result.append(jumpChar(ch, jumpAmt));
        }

        return new Spell(result.toString(), spell.getCodex());
    }

    private char jumpChar(char target, int jump) {
        return (char) (target + jump);
    }
}

