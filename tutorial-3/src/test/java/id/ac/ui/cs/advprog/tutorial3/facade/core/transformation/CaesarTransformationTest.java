package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CaesarTransformationTest {
    private Class<?> caesarClass;

    @BeforeEach
    public void setup() throws Exception {
        caesarClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.CaesarTransformation");
    }

    @Test
    public void testCelestialHasEncodeMethod() throws Exception {
        Method translate = caesarClass.getDeclaredMethod("encode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testCelestialEncodesCorrectly() throws Exception {
        String text = "ABCDEFG";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "BCDEFGH";

        Spell result = new CaesarTransformation().encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testCelestialEncodesCorrectlyWithCustomKey() throws Exception {
        String text = "ABCDEFG";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "DEFGHIJ";

        Spell result = new CaesarTransformation(3).encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testCelestialHasDecodeMethod() throws Exception {
        Method translate = caesarClass.getDeclaredMethod("decode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testCelestialDecodesCorrectly() throws Exception {
        String text = "bcdefg";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "abcdef";

        Spell result = new CaesarTransformation().decode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testCelestialDecodesCorrectlyWithCustomKey() throws Exception {
        String text = "cdefgh";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "abcdef";

        Spell result = new CaesarTransformation(2).decode(spell);
        assertEquals(expected, result.getText());
    }
}
